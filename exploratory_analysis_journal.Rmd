---
title: "Exploratory analysis for journalists Twitter activity"
output:
  html_notebook: default
  pdf_document: default
---

```{r setup, include=FALSE}

knitr::opts_chunk$set(echo = FALSE, cache = TRUE, cache.lazy = FALSE)

library(stringr)
library(tidyverse)
library(stats)
library(lubridate)
library(scales)
library(knitr)
library(ggrepel)
library(broom)
library(tidyselect)
library(xtable)
library(kableExtra)
#library(ggmap)
#library(geosphere) # for computing the haversine distance
#library(rgdal)
#library(raster)
library(RColorBrewer)
#library(rgeos)
#library(ggfortify)
#library(cluster) # library needed for PAM clustering

source("../twitter_common/twitter_utilities.R")

cbPalette2 <-c("#d7191c", "#abdda4", "#2b83ba", "#fdae61")
old_theme <- theme_set(theme_minimal(base_size = 16, base_family = "Helvetica"))
#old_theme <- theme_set(theme_grey(base_size = 22) + theme_minimal(base_size = 22, base_family = "Helvetica"))
my_palette <- colorRampPalette(c("white", "green", "yellow", "red"))(299)

```

# Load data

```{r load_and_parse_profiles, echo=FALSE, message=FALSE}
profiles_df <- read_profiles("/Users/chiara/Documents/Code/python/twitter_monitor_journalists/datasets/Stampa_Tweet-giornalisti_profiles.tsv")

profiles_df <- 
  profiles_df %>%
  filter(statuses_count > 1)

profiles_df$user_id <- as.character(profiles_df$user_id)
```


```{r load_and_parse_timelines, echo=FALSE, message=FALSE, cache=TRUE}

timelines_df <- read_timelines("/Users/chiara/Documents/Code/python/twitter_monitor_journalists/datasets/Stampa_Tweet-giornalisti_timelines.tsv")

timelines_df$user_id <- as.character(timelines_df$user_id)

```

```{r load_egonets, echo=FALSE, message=FALSE}

contact_type <- "all"

ego_stats <- read.table(paste("/Users/chiara/Documents/Code/python/journalists_analysis/results/stats/italian/journalists_s_filter_", contact_type, ".csv", sep = ""),
                           header = TRUE, row.names = NULL, stringsAsFactors = FALSE, sep = " ",
                           comment.char = "", quote = "") %>%
  rename(user_id = ego_id)

ego_stats$is_human <- as.logical(ego_stats$is_human)
ego_stats$user_id <- as.character(ego_stats$user_id)



#ego_stats <- left_join(ego_stats, dplyr::select(timeline_properties_for_clust, user_id, is_outlier_observed), by = "user_id")

# ego_stats <- 
#   left_join(ego_stats, dplyr::select(clustering_dbscan, user_id, clust), by = "user_id") %>%
#   mutate(is_outlier_observed = case_when(clust == 0 ~ TRUE, TRUE ~ FALSE))

# length(unique(ego_stats$user_id))
# 
# summary(ego_stats)

alters_to_circles <- read.table(
  paste("/Users/chiara/Documents/Code/python/journalists_analysis/results/stats/italian/circles_s_filter_", contact_type, ".csv", sep = ""),
  header = TRUE, row.names = NULL, stringsAsFactors = FALSE, sep = " ",
  comment.char = "", quote = "") %>%
  rename(user_id = ego_id)

alters_to_circles$user_id <- as.character(alters_to_circles$user_id)
alters_to_circles$alter_id <- as.character(alters_to_circles$alter_id)

```

# The dataset

We downloaded the timeline (last 3200 tweets) for the journalists belonging to the Italian journalists list at [link](https://twitter.com/stampa_tweet/lists/giornalisti). This list comprises 492 journalists that are the most popular in Italy. Of these 492 members, we discard those with a protected timeline or with zero or one tweets, ending up with `r nrow(profiles_df)` users.


```{r statuses_plots, echo = FALSE, message=FALSE, warning=FALSE}

ggplot(profiles_df) + geom_histogram(aes(x = statuses_count), bins = 50, color = "black") + xlab("Total tweets in active lifetime")

ggplot(profiles_df, aes(twitter_age_months * 30 / 365 , statuses_count)) + geom_point(color = cbPalette2[3]) +
  geom_smooth() + #scale_x_continuous(limits = c(5,12)) + 
  xlab("Twitter age [years]") + ylab("Tweets since Twitter birth")


```

```{r load_categories}

# italian politicians
profiles_politicians <-read_profiles("~/Documents/Code/python/twitter_monitor_journalists/politicians_profiles/all_politicians.tsv") %>%
  group_by(user_id) %>%
  slice(1L) %>%
  ungroup()

profiles_politicians$user_id <- as.character(profiles_politicians$user_id)

# several newspapers around the world
profiles_newspapers <-read_profiles("~/Documents/Code/python/twitter_monitor_journalists/newspapers_profiles/all_newspapers.tsv") %>%
  group_by(user_id) %>%
  slice(1L) %>%
  ungroup()

profiles_newspapers$user_id <- as.character(profiles_newspapers$user_id) 

# all journalists in our international dataset
profiles_journalists_all <-read_profiles("~/Documents/Code/python/twitter_monitor_journalists/journalists_profiles/all_journalists.tsv") %>%
  group_by(user_id) %>%
  slice(1L) %>%
  ungroup() %>%
  filter(user_id != "121190265")

profiles_journalists_all$user_id <- as.character(profiles_journalists_all$user_id) 

# all alters - TBC
profiles_alters <-read_profiles("~/Documents/Code/python/twitter_monitor_journalists/test_profiles.tsv") %>%
  group_by(user_id) %>%
  slice(1L) %>%
  ungroup()

profiles_alters$user_id <- as.character(profiles_alters$user_id) 

# create a dataframe containing all categories
categories_labelled <- bind_rows(mutate(dplyr::select(profiles_journalists_all, alter_id = user_id, alter_name = name), alter_label = "journalists_all" ),
                                 mutate(dplyr::select(profiles_politicians, alter_id = user_id,  alter_name = name), alter_label = "politicians_ita" ),
                                 mutate(dplyr::select(profiles_newspapers, alter_id = user_id,  alter_name = name), alter_label = "news" ))

# check that there are no duplicates in different categories
categories_labelled %>%
  group_by(alter_id, name) %>%
  mutate(tot = n()) %>%
  filter(tot > 1) %>%
  arrange(alter_id)

```


```{r timeline_properties, echo=FALSE, message=FALSE}

timeline_properties_df <- get_timeline_properties_and_filter(timelines_df, profiles_df)

timeline_summary <- get_timeline_summary(timelines_df, profiles_df, timeline_properties_df)

# keep only active and regular users (OUTPUT)
timelines_df <- semi_join(timelines_df,
                          filter(timeline_properties_df, abandonment_status == "active" & user_type == "regular"), by = "user_id")

cat(paste("Users active and regular: ", length(unique(timelines_df$user_id)), sep=""))

# get alters
timelines_with_alters <- get_timeline_with_alters(timelines_df) %>%
  filter(tweet_type != "INDIRECT")

timelines_with_alters$alter_id <- as.character(timelines_with_alters$alter_id) 

rm(timelines_df)
```

# Interactions distributed per alter category

We classify alters, when possible, in journalists, news outlets, and politicians (ITA). 

```{r interactions_labelled, echo=FALSE, message=FALSE}
interactions_labelled <- left_join(timelines_with_alters, categories_labelled, by = "alter_id") %>%
  mutate(alter_label = case_when(is.na(alter_label) ~ "others", TRUE ~ alter_label)) %>%
  dplyr::select(id, user_id, alter_id, name, alter_label) %>%
  group_by(user_id, alter_label) %>%
  summarise(perc_category = n()) %>%
  group_by(user_id) %>%
  mutate(perc_category = perc_category/sum(perc_category) * 100)

interactions_labelled %>%
  group_by(alter_label) %>%
  summarise(avg = mean(perc_category)) %>%
  ggplot(.) + geom_histogram(data = interactions_labelled, aes(perc_category)) + 
  geom_vline(aes(xintercept = avg), color = "red", linetype = 2) + geom_label(aes(x = avg+10, y = 150, label = round(avg,2)), color = "red") +
  facet_wrap(~ alter_label) + xlab("% in category") + ylab("Tot users")


# dplyr::select(test, alter_id) %>%
#   distinct(alter_id) %>%
#   write_tsv(., path = "~/Documents/Code/python/twitter_monitor_journalists/test.tsv")
```

# Employer distribution


```{r getting_the_employer}

# # this generates the file for manual tagging
# journalists_with_employer <-
#   profiles_df %>%
#   dplyr::select(user_id, name, description)
# 
# write_tsv(journalists_with_employer, path = "journalists_with_employer.tsv")

# journalists_with_employer$employer[which(str_detect(journalists_with_employer$description, "Repubblica"))] <- "repubblica"
# journalists_with_employer$employer[which(str_detect(journalists_with_employer$description, "@la_stampa"))] <- "LaStampa"
# journalists_with_employer$employer[which(str_detect(journalists_with_employer$description, "Il Giornale"))] <- "ilgiornale"
# journalists_with_employer$employer[which(str_detect(journalists_with_employer$description, "Espresso"))] <- "espressonline"
# journalists_with_employer$employer[which(str_detect(journalists_with_employer$description, "Gazzetta dello Sport"))] <- "espressonline"


# j_pattern <- paste(profiles_newspapers$screen_name, collapse = "|")
# journalists_with_employer$employer <- str_match(journalists_with_employer$description, j_pattern)

# load the manually tagged df of journalists + media they work for
journalists_with_employer <- read_tsv( "journalists_with_employer.tsv") %>%
  filter(name != "24finanza" & name != "24previdenza") %>%
  mutate(user_id = as.character(user_id)) %>%
  semi_join(., filter(timeline_properties_df, abandonment_status == "active" & user_type == "regular"), by = "user_id")
journalists_with_employer$employer[which(str_detect(journalists_with_employer$employer, "Sky"))] <- "Sky"
journalists_with_employer$popularity[which(is.na(journalists_with_employer$popularity))] <- 1

employer_classification <- read_tsv("employer_classification.tsv")

employer_classification$where[which(is.na(employer_classification$where))] <- "others"

employer_classification %>%
  ggplot(.) + geom_bar(aes(where))  + theme(axis.text.x = element_text(angle = 60, hjust = 1)) + ggtitle("Media category")


journalists_with_employer_pop <- journalists_with_employer %>%
  left_join(dplyr::select(profiles_df, user_id, statuses_count, followers_count, friends_count), by = "user_id") %>%
  left_join(dplyr::select(timeline_summary, user_id, social_tweets, tot_tweets), by = "user_id") %>%
  left_join(employer_classification, by = "employer")



```

```{r}

journalists_with_employer_pop %>%
  group_by(employer) %>%
  summarise(tot = n()) %>%
  top_n(10, tot) %>%
  arrange(tot) %>%
  ggplot(.) + geom_col(aes(employer, tot)) + theme(legend.position = "none", axis.text.x = element_text(angle = 45, hjust = 1)) +
  ggtitle("Top 10 employers") +
  ylab("# journalists") + xlab("") #+ scale_fill_brewer(name = "", palette ="RdYlBu")


ggplot(journalists_with_employer_pop) +
  geom_point(aes(statuses_count * social_tweets/tot_tweets , followers_count/1000, color = as.factor(popularity), size = popularity), alpha = 0.7) +
  theme(legend.position = "top") + 
  scale_x_log10() + scale_y_log10() +
  scale_color_brewer(name = "Popularity", palette ="RdYlBu", direction = -1) +
  xlab("# social tweets") + ylab("# followers [*10^3]") +
  guides(size=FALSE) + geom_vline(xintercept = 3200, color = "grey55", linetype = 2) +
  geom_text_repel(data = filter(journalists_with_employer_pop, popularity == 5),
                  aes(statuses_count * social_tweets/tot_tweets , followers_count/1000, label = name), size = 3)

ggplot(journalists_with_employer_pop) + 
  geom_point(aes(statuses_count * social_tweets/tot_tweets, followers_count/1000, color = as.factor(popularity), size = popularity), alpha = 0.7) +
  theme(legend.position = "top") + 
  #scale_x_log10() + scale_y_log10() +
  scale_color_brewer(name = "Popularity", palette ="RdYlBu", direction = -1) +
  xlab("# social tweets") + ylab("# followers [*10^3]") +
  guides(size=FALSE) + geom_vline(xintercept = 3200, color = "grey55", linetype = 2) #+
  #geom_text_repel(data = filter(journalists_with_employer_pop, popularity == 5), aes(statuses_count, followers_count/1000, label = name), size = 3)

journalists_with_employer_pop %>%
  ggplot(., aes(statuses_count * social_tweets/tot_tweets, followers_count, color = as.factor(popularity))) +
  geom_point(alpha = 0.7) + geom_smooth() +
  theme(legend.position = "top") + #facet_wrap(~ popularity, scales = "free") +
  scale_x_log10() + scale_y_log10() + scale_color_brewer(name = "Popularity", palette ="RdYlBu") + xlab("# tweets") +
  ylab("# followers") + guides(size=FALSE)

journalists_with_employer_pop %>%
  ggplot(.) + geom_col(aes(as.factor(popularity), mean(followers_count), fill = as.factor(popularity))) + theme(legend.position = "none") + 
  scale_color_discrete(name = "Popularity") + xlab("Popularity") + ylab("Average # followers")

```

# Popularity

I have manually tagged the journalists (5: most popular, 1: least popular).

```{r}
journalists_with_employer_pop %>%
  group_by(where) %>%
  filter(n() > 10) %>%
  ungroup() %>%
  ggplot(., aes(followers_count, popularity)) + geom_point(aes(color = as.factor(popularity), size = popularity), alpha = 0.7) +
  geom_smooth() +  theme(legend.position = "top") + 
  scale_x_log10() + xlab("# followers") + ylab("Popularity") + guides(size=FALSE) +
  scale_color_brewer(name = "Popularity", palette ="RdYlBu", direction = -1)

```

The correlation between number of followes and popularity is `r cor(journalists_with_employer_pop$followers_count, journalists_with_employer_pop$popularity)`. We can also compute it per media category.

```{r}
journalists_with_employer_pop %>%
  group_by(where) %>%
  filter(n() > 10) %>% 
  do(data.frame(r=cor(.$followers_count,.$popularity)))
```


```{r fig.height=7, fig.width=10}
journalists_with_employer_pop %>%
  group_by(where) %>%
  filter(n() > 10) %>%
  ungroup() %>%
  ggplot(.) + geom_point(aes(followers_count, popularity, color = as.factor(popularity), size = popularity), alpha = 0.7) +
  theme(legend.position = "top") + 
  scale_x_log10() + xlab("# social tweets") + ylab("# followers") + guides(size=FALSE) +
  facet_wrap(~ where) + scale_color_brewer(name = "Popularity", palette ="RdYlBu", direction = -1)

```

```{r}
journalists_with_employer_pop %>%
  filter(where == "tv") %>%
  arrange(desc(followers_count)) %>%
  dplyr::select(name, followers_count, popularity) %>%
  slice(1:20)
```


```{r fig.height=7, fig.width=10}
journalists_with_employer_pop %>%
  group_by(where) %>%
  filter(n() > 10) %>%
  ungroup() %>%
  ggplot(.) + geom_point(aes(statuses_count * social_tweets/tot_tweets , followers_count, color = as.factor(popularity), size = popularity), alpha = 0.7) +
  theme(legend.position = "top") + 
  scale_x_log10() + scale_y_log10()  + xlab("# social tweets") + ylab("# followers") + guides(size=FALSE) +
  facet_wrap(~ where) + scale_color_brewer(name = "Popularity", palette ="RdYlBu", direction = -1)
```



```{r fig.height=6, fig.width=12}
# journalists_with_employer %>%
#   left_join(dplyr::select(profiles_df, user_id, statuses_count, followers_count, friends_count), by = "user_id") %>%
#   left_join(employer_classification, by = "employer") %>%
#   ggplot(.) + geom_col(aes(as.factor(where), mean(followers_count), fill = as.factor(where))) +
#   theme(legend.position = "none", axis.text.x = element_text(angle = 60, hjust = 1)) + 
#   scale_color_discrete(name = "Popularity") + xlab("Popularity") + ylab("Average # followers")

journalists_with_employer_pop %>%
  ggplot(.) + geom_jitter(aes(as.factor(where), followers_count, color = as.factor(where))) +
  theme(legend.position = "none", axis.text.x = element_text(angle = 60, hjust = 1)) + 
  scale_color_discrete(name = "Popularity") + xlab("Popularity") + ylab("Average # followers")

journalists_with_employer_pop %>%
  ggplot(.) + geom_boxplot(aes(as.factor(where), followers_count, color = as.factor(where)), outlier.alpha = 0.5) +
  theme(legend.position = "none", axis.text.x = element_text(angle = 60, hjust = 1)) + #scale_y_log10() +
  scale_color_discrete(name = "Popularity") + xlab("") + ylab("Average # followers")

```

```{r}

journalists_with_employer_pop %>%
  group_by(where) %>%
  filter(n() > 10) %>%
  ungroup() %>%
  filter(where == "newspaper" | where == "magazine") %>%
  mutate(type = case_when(is.na(type) ~ "both", TRUE ~ type)) %>%
  ggplot(.) + geom_point(aes(statuses_count * social_tweets/tot_tweets, followers_count, color = as.factor(type), size = popularity), alpha = 0.7) +
  theme(legend.position = "top") + 
  scale_x_log10() + scale_y_log10()  + xlab("# social tweets") + ylab("# followers") + guides(size=FALSE) +
  facet_wrap(~ where) + scale_color_brewer(name = "Type", palette ="Set1", direction = 1)

journalists_with_employer_pop %>%
  group_by(where) %>%
  filter(n() > 10) %>%
  ungroup() %>%
  filter(where == "newspaper") %>%
  mutate(scope = case_when(is.na(scope) ~ "national", TRUE ~ scope)) %>%
  ggplot(.) + geom_point(aes(statuses_count * social_tweets/tot_tweets, followers_count, color = as.factor(scope), size = popularity), alpha = 0.7) +
  theme(legend.position = "top") + 
  scale_x_log10() + scale_y_log10()  + xlab("# social tweets") + ylab("# followers") + guides(size=FALSE) +
  facet_wrap(~ where) + scale_color_brewer(name = "Type", palette ="Set1", direction = 1)
```

# Ego networks

```{r}
journalists_with_employer_ego <- left_join(journalists_with_employer_pop, ego_stats, by = "user_id")

```

```{r total_network_size, fig.height=6, fig.width=6}
journalists_with_employer_ego %>%
  group_by(where) %>%
  filter(n() > 20) %>%
  ungroup() %>%
  filter(activenet_size > 0) %>%
  ggplot(., aes(followers_count, total_size, color = as.factor(popularity), group = where, size = as.factor(popularity))) +
  geom_point(alpha = 0.7) +  geom_smooth() +
  theme(legend.position = "top") + 
  ylab("Total network size") + xlab("# followers") + guides(size=FALSE) + scale_x_log10() +
  facet_wrap(~ where) + scale_color_brewer(name = "Popularity", palette ="Set1", direction = -1) + ggtitle("Total network size")

journalists_with_employer_ego %>%
  group_by(where) %>%
  filter(n() > 20) %>%
  ungroup() %>%
  filter(activenet_size > 0) %>%
  ggplot(., aes(followers_count, total_size)) +
  geom_point(aes(color = as.factor(popularity), size = as.factor(popularity)), alpha = 0.7) +  geom_smooth() +
  theme(legend.position = "top") + 
  ylab("Total network size") + xlab("# followers") + guides(size=FALSE) + scale_x_log10() +
  scale_color_brewer(name = "Popularity", palette ="Set1", direction = -1) + ggtitle("Total network size")

```


```{r active_network, fig.height=6, fig.width=6}
journalists_with_employer_ego %>%
  group_by(where) %>%
  filter(n() > 20) %>%
  ungroup() %>%
  filter(activenet_size > 0) %>%
  ggplot(., aes(followers_count, activenet_size, color = as.factor(popularity), group = where, size = as.factor(popularity))) +
  geom_point(alpha = 0.7) + geom_smooth(show.legend = FALSE) +
  theme(legend.position = "top") + 
  ylab("Active network size") + xlab("# followers") + guides(size=FALSE) + scale_x_log10() +
  facet_wrap(~ where) + scale_color_brewer(name = "Popularity", palette ="Set1", direction = -1) + ggtitle("Active network size")

journalists_with_employer_ego %>%
  group_by(where) %>%
  filter(n() > 20) %>%
  ungroup() %>%
  filter(activenet_size > 0) %>%
  ggplot(., aes(followers_count, activenet_size)) +
  geom_point(aes(color = as.factor(popularity), size = as.factor(popularity)), alpha = 0.7) + geom_smooth(show.legend = FALSE) +
  theme(legend.position = "top") + 
  ylab("Active network size") + xlab("# followers") + guides(size=FALSE) + scale_x_log10() +
  scale_color_brewer(name = "Popularity", palette ="Set1", direction = -1) + ggtitle("Active network size")


journalists_with_employer_ego %>%
  group_by(where) %>%
  filter(n() > 20) %>%
  ungroup() %>%
  filter(activenet_size > 0) %>%
  ggplot(., aes(followers_count, activenet_size)) +
  geom_point(alpha = 0.7) + geom_smooth(show.legend = FALSE) +
  theme(legend.position = "top") + 
  ylab("Active network size") + xlab("# followers") + guides(size=FALSE) + scale_x_log10() +
  scale_color_brewer(name = "Popularity", palette ="Set1", direction = -1) + ggtitle("Active network size")


```

```{r num_circles, fig.height=6, fig.width=6}
journalists_with_employer_ego %>%
  group_by(where) %>%
  filter(n() > 20) %>%
  ungroup() %>%
  filter(activenet_size > 0) %>%
  ggplot(., aes(followers_count, n_circles, color = as.factor(popularity), group = where, size = as.factor(popularity))) +
  geom_point(alpha = 0.7) + geom_smooth(show.legend = FALSE) +
  theme(legend.position = "top") + 
  ylab("Optimal number of circles") + xlab("# followers") + guides(size=FALSE) + scale_x_log10() +
  facet_wrap(~ where) + scale_color_brewer(name = "Popularity", palette ="Set1", direction = -1) + ggtitle("Optimal number of circles")

journalists_with_employer_ego %>%
  group_by(where) %>%
  filter(n() > 20) %>%
  ungroup() %>%
  filter(activenet_size > 0) %>%
  ggplot(., aes(followers_count, n_circles)) +
  geom_point(aes(color = as.factor(popularity), size = as.factor(popularity)), alpha = 0.7) + geom_smooth(show.legend = FALSE) +
  theme(legend.position = "top") + 
  ylab("Optimal number of circles") + xlab("# followers") + guides(size=FALSE) + scale_x_log10() +
  scale_color_brewer(name = "Popularity", palette ="Set1", direction = -1) + ggtitle("Optimal number of circles")


```

## Labelled alters in circles

```{r}

interactions_labelled_per_circle <- left_join(alters_to_circles, categories_labelled, by = "alter_id") %>%
  mutate(alter_label = case_when(is.na(alter_label) ~ "others", TRUE ~ alter_label)) %>%
  semi_join(filter(ego_stats, activenet_size > 0), by = "user_id")

temp <- interactions_labelled_per_circle %>%
  group_by(user_id, circle_id, alter_label) %>%
  summarise(tot_in_circle = n())

```

```{r}

data_frame(user_id = rep(unique(temp$user_id), each = 5 * length(unique(temp$alter_label))),
           circle_id = rep(1:5, each = length(unique(temp$alter_label)), times = length(unique(temp$user_id)) ),
           alter_label = rep(unique(temp$alter_label), times = 5 * length(unique(temp$user_id)))) %>%
  left_join(temp, by = c("user_id", "circle_id", "alter_label")) %>%
  replace_na(list(tot_in_circle = 0)) %>%
  #group_by(user_id, circle_id, alter_label) %>%
  #summarise(tot_in_circle = n()) %>%
  group_by(circle_id, alter_label) %>%
  summarise(avg_in_circle = mean(tot_in_circle)) %>%
  group_by(circle_id) %>%
  mutate(avg_in_circle = avg_in_circle/sum(avg_in_circle)) %>%
  ggplot(.) + geom_col(aes(circle_id, avg_in_circle, fill = as.factor(alter_label))) +
  scale_fill_brewer(name = "Alter category", palette ="Set1", direction = -1)  + theme(legend.position = "top") +
  xlab("Ring ID") + ylab("") + scale_y_continuous(labels = scales::percent)




data_frame(user_id = rep(unique(temp$user_id), each = 5 * length(unique(temp$alter_label))),
           circle_id = rep(1:5, each = length(unique(temp$alter_label)), times = length(unique(temp$user_id)) ),
           alter_label = rep(unique(temp$alter_label), times = 5 * length(unique(temp$user_id)))) %>%
  left_join(temp, by = c("user_id", "circle_id", "alter_label")) %>%
  replace_na(list(tot_in_circle = 0)) %>%
  group_by(circle_id, alter_label) %>%
  summarise(avg_in_circle = mean(tot_in_circle)) %>%
  #group_by(circle_id) %>%
  #mutate(avg_in_circle = avg_in_circle/sum(avg_in_circle)) %>%
  ggplot(.) + geom_col(aes(circle_id, avg_in_circle, fill = as.factor(alter_label))) +
  scale_fill_brewer(name = "Alter category", palette ="Set1", direction = -1)  + theme(legend.position = "top") +
  xlab("Ring ID") + ylab("Avg # alters in circle") #+
  #geom_hline(yintercept = mean(journalists_with_employer_ego$size_C1), color = "grey55", linetype = 2)


```

# Alter profiles analysis

```{bash}

scp chiara@turig.iit.cnr.it:~/python/twitter_monitor_journalists/test_profiles.tsv ./alters_profiles.tsv
```


```{r}
alters_profiles <- read_tsv("alters_profiles.tsv") %>%
  rename(alter_id = id) %>%
  mutate(alter_id = as.character(alter_id)) %>%
  dplyr::select(alter_id, name, description, followers_count, friends_count) %>%
  left_join(categories_labelled, by = "alter_id")

test_match <- left_join(dplyr::select(alters_profiles, alter_id, name, description), categories_labelled, by = "alter_id") %>%
  group_by(alter_label) %>%
  summarise(tot = n())

alters_profiles %>%
  filter(is.na(alter_label)) %>%
  arrange(desc(followers_count)) %>%
  mutate(new_label = str_match(str_to_lower(description),
                               "giornalista|telecronista|cronista|inviato|politico|deputato|deputata|senatore|senatrice|ministro|scrittore|scrittrice")) %>%
  filter(is.na(new_label)) %>%
  View()
#  group_by(new_label) %>%
#  summarise(tot = n())
  
alters_profiles %>%
  ggplot(.) + stat_ecdf(aes(followers_count, y = 1 - ..y..), color = "red") + stat_ecdf(aes(friends_count, y = 1 - ..y..), color = "blue") +
  scale_x_log10( breaks = exp(seq(log(1), log(10^8), log(10))), labels = c("10^0", "10^1", "10^2", "10^3", "10^4","10^5","10^6","10^7","10^8")) + scale_y_log10()
```

IDEA: si potrebbero classificare come VIP gli alter che stanno sopra un certo numbero di followers.
